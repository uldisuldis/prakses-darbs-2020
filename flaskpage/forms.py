from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, IntegerField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError

class MessageForm(FlaskForm):
	name = StringField('Vārds Uzvārds', validators=[DataRequired()])
	title = StringField('Temats', validators=[DataRequired()])
	content = TextAreaField('Ziņojums', validators=[DataRequired()])
	submit = SubmitField('Send')

	def validate_email(self, name):
		if false:
			raise ValidationError('No account found with that email.')
