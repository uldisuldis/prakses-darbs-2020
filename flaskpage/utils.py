from flask_mail import Message
from flaskpage import mail

def send_message(sender, title, content):
    msg = Message(repr({title})[2:-2]+' <'+repr({sender})[2:-2]+'>',
            sender='noreply@va.lv',
            recipients=['uldis.kuzma@gmail.com'])
    msg.body = f'''
{content}
'''
    mail.send(msg)
