from flask import render_template, url_for, flash, abort, redirect, request, Blueprint
from flaskpage.utils import send_message
from flaskpage.forms import (MessageForm)

mainBP = Blueprint('main', __name__)
errorsBP = Blueprint('errors', __name__)

@mainBP.route("/")
@mainBP.route("/about")
def about():
	return render_template('about.html', title='Par')

@mainBP.route("/")
@mainBP.route("/contact", methods=['GET', 'POST'])
def contact():
	form = MessageForm()
	if form.validate_on_submit():
		send_message(form.name.data, form.title.data, form.content.data)
		flash('Ziņojums nosūtīts!', 'success')
		return redirect(url_for('main.contact'))
	elif request.method == 'GET':
		return render_template('contact.html', title='Kontakti', form=form)

@errorsBP.app_errorhandler(404)
def error_404(error):
	return render_template('errors/404.html'), 404

@errorsBP.app_errorhandler(403)
def error_403(error):
	return render_template('errors/403.html'), 403

@errorsBP.app_errorhandler(500)
def error_500(error):
	return render_template('errors/500.html'), 500
