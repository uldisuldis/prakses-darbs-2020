from flask import Flask
#from flask_sqlalchemy import SQLAlchemy
#from flask_bcrypt import Bcrypt
#from flask_login import LoginManager
from flask_mail import Mail
from flaskpage.config import Config

#db = SQLAlchemy()
#bcrypt = Bcrypt()
#login_manager = LoginManager()
#login_manager.login_view = 'users.login'
#login_manager.login_message_category = 'info'
mail = Mail()


def create_app(Config):
    app = Flask(__name__)
    app.config.from_object(Config)

    #db.init_app(app)
    #bcrypt.init_app(app)
    #login_manager.init_app(app)
    mail.init_app(app)

    from flaskpage.routes import mainBP, errorsBP
    app.register_blueprint(mainBP)
    app.register_blueprint(errorsBP)

    return app
